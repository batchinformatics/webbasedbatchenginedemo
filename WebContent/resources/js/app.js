var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connectbatchlist() {
    var socket = new SockJS('/CS700/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/batchchanges', function (message) {
            var batch = JSON.parse(message.body);
            updateBatchList(batch);
        });
    });
}

function connectbatchdetails() {
	displayorhideinstructions();
	var bsn = document.getElementById("BSN").innerHTML;
    var socket = new SockJS('/CS700/ws');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/bsn'+bsn, function (message) {
            var batch = JSON.parse(message.body);
            if (batch.batchStatus == 'DELETED'){
            	window.location.href = '/CS700/batchList';
            }
            else {
                if (batch.batchStatus != document.getElementById("batchStatus").innerHTML){
                    updateBatchHeader(batch);
                }
            	updateInstruction(batch);            	
            }
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function ackMsg() {
	var bsn = document.getElementById("BSN").innerHTML;
	var isn = document.getElementById("ISN").innerHTML;
    stompClient.send("/CS700/ackMsg", {}, JSON.stringify({'bsn': bsn,'isn': isn }));
}
function statusCommand(bsn,command) {
    stompClient.send("/CS700/batchCommand", {}, JSON.stringify({'bsn': bsn,'command': command }));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

function removeOptions(selectbox)
{
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}

function updateBatchHeader(batch) {
    document.getElementById("batchStatus").innerHTML=batch.batchStatus;
    var select = document.getElementById('availableCommands');
    removeOptions(select);
    var selectHead = document.createElement("option");
    selectHead.textContent = "Select a command for this batch";
    select.appendChild(selectHead);
    var i;
    for (i = 0; i < batch.listCommands.length; i++) {
        var opt = batch.listCommands[i];
        var el = document.createElement("option");
        el.textContent = opt;
        el.value = opt;
        select.appendChild(el);		
		console.log(batch.listCommands[i]);
	}
}

function updateBatchList(batch) {
    var row = document.getElementById(batch.batchSeq);
	if (batch.batchStatus == 'DELETED') {
	    row.parentNode.removeChild(row);
	}
	else {
		if (row == null){
			var table = document.getElementById('batchlist');
			row = table.insertRow(0);
			row.setAttribute("id",batch.batchSeq);
			var batchidcol = row.insertCell(0);
			var recipenamecol = row.insertCell(1);
			var batchstatuscol = row.insertCell(2);
			var commandcol = row.insertCell(3);
			a = document.createElement('a');
		    a.href =  "batchDetails?BSN=" + batch.batchSeq; // Insted of calling setAttribute 
		    a.innerHTML = batch.batchId // <a>INNER_TEXT</a>
		    batchidcol.appendChild(a); // Append the link to the div
			//batchidcol.innerHTML = batch.batchId;
			recipenamecol.innerHTML = batch.recipeName;
			var selectlist = document.createElement("select");
			selectlist.setAttribute("onchange","statusCommand("+batch.batchSeq+",this.value)");
			commandcol.appendChild(selectlist);
		}
	    row.cells[2].innerHTML = batch.batchStatus;
	    var select = row.cells[3].getElementsByTagName("select")[0];
	    removeOptions(select);
	    var selectHead = document.createElement("option");
	    selectHead.textContent = "Select a command for this batch";
	    select.appendChild(selectHead);
	    var i;
	    for (i = 0; i < batch.listCommands.length; i++) {
	        var opt = batch.listCommands[i];
	        var el = document.createElement("option");
	        el.textContent = opt;
	        el.value = opt;
	        select.appendChild(el);		
			console.log(batch.listCommands[i]);
		}		
	}
}


function updateInstruction(batch) {
	  for (i = 0; i < batch.listInstructions.length; i++) 
	  {
	      if (batch.listInstructions[i].acknowledged == "A") 
	      {
	        document.getElementById("ISN").innerHTML = batch.listInstructions[i].instructionSeq;
	    	document.getElementById("instructtextbox").innerHTML = batch.listInstructions[i].instructText;
	      } 
	  }
	  displayorhideinstructions();
}

function displayorhideinstructions() {
	var batchStatus = document.getElementById("batchStatus").innerHTML;
	switch (batchStatus){
	case "ABORTED":
	case "READY":
	case "CLOSED":
	case "DONE":
		  document.getElementById('instructionsdiv').style.display="none";
		  break;
	case "HELD":
		  document.getElementById('instructionsdiv').style.display="block";
		  document.getElementById('instructbutton').style.visibility="hidden";
		  break;
	case "RUNNING":
		  document.getElementById('instructionsdiv').style.display="block";
		  document.getElementById('instructbutton').style.visibility="visible";
		  break;
	}
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#ack" ).click(function() { ackMsg(); });

});

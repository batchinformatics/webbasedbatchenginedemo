<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/cs700style.css" />" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create New Batch</title>
</head>
<body>
 <section id="header">
    <h1>CIS 700B</h1>
    <p>Will Bassett</p>
    <p>Create Batch Page</p>
  </section>
  <section id="navigator">
    <h1>Navigation panel</h1>
    <ul>
      <li><a href="batchList">Batch List</a><br></li>
    </ul>
  </section>
  <section id = "content"> 
    <form action="newBatch">
      <fieldset>
        <legend>New Batch Information:</legend>
        Enter a unique batch identifier:<br>
        <input type="text" name="BATCH_ID" value="">
        <br>
        Select a recipe to initiate:<br>
        <select name="RECIPE_NAME">
          <option value="">Choose a recipe to create a new batch</option>
          <c:forEach var="recipe" items="${recipeList}">
            <option value="${recipe.recipeName}">Recipe Name:${recipe.recipeName} Recipe Ver: ${recipe.recipeVer} Recipe Desc: ${recipe.recipeDesc}</option>
          </c:forEach>
        </select>
        <br><br>
        <input type="submit" value="Submit">
      </fieldset>
    </form>
  </section>
</body>
</html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/cs700style.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/sockjs-client/sockjs.min.js"/>"></script>
    <script src="<c:url value="/resources/js/stomp-websocket/stomp.min.js"/>"></script>
    <script src="<c:url value="/resources/js/app.js"/>"></script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Batch Detail</title>
</head>
<body>
  <section id="header">
    <h1>CIS 700B</h1>
    <p>Will Bassett</p>
    <p>Batch Details Page</p>
  </section>
  <section id="navigator">
    <h1>Navigation panel</h1>
    <ul>
      <li><a href="batchList">Batch List</a><br></li>
      <li><a href="createBatch">Create New Batch</a></li>
    </ul>
  </section>
  <section id = "content">
  <div>  
    <p id="BSN">${batch.batchSeq}</p>
    <table>
      <thead>
        <th>Batch ID</th>
        <th>Recipe Name</th>
        <th>Batch Status</th>
        <th>Command</th>
      </thead>
      <tr>
        <td>${batch.batchId}</td>
        <td>${batch.recipeName}</td>
        <td id="batchStatus">${batch.batchStatus}</td>
        <td><select id="availableCommands" onchange="statusCommand(${batch.batchSeq},this.value)">
              <option value = "">Select a command for this batch</option>
              <c:forEach var="command" items="${batch.listCommands}">
              <option value="${command}">${command}</option>
              </c:forEach>
        </select></td> 
      </tr>
    </table>
  </div>
  <div id = "instructionsdiv">
    <div id = "instructbutton"> 
      <button id="ack" type="submit">Acknowledge</button>
    </div> 
    <div id = "instructtext">
          <p id="ISN"><c:forEach var="instruction" items="${batch.listInstructions}"><c:if test="${instruction.acknowledged == 'A'}">${instruction.instructionSeq}</c:if></c:forEach></p>
          <textarea id ="instructtextbox" readonly><c:forEach var="instruction" items="${batch.listInstructions}"><c:if test="${instruction.acknowledged == 'A'}">${instruction.instructText}</c:if></c:forEach></textarea>
    </div>
  </div>
  </section>
</body>
<body onload="connectbatchdetails()">
</html>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/cs700style.css" />" rel="stylesheet">
    <script src="<c:url value="/resources/js/jquery/jquery.min.js"/>"></script>
    <script src="<c:url value="/resources/js/sockjs-client/sockjs.min.js"/>"></script>
    <script src="<c:url value="/resources/js/stomp-websocket/stomp.min.js"/>"></script>
    <script src="<c:url value="/resources/js/app.js"/>"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Batch List</title>
</head>
<body onload="connectbatchlist()">
 <section id="header">
    <h1>CIS 700B</h1>
    <p>Will Bassett</p>
    <p>Batch List Page</p>
  </section>
  <section id="navigator">
    <h1>Navigation panel</h1>
    <ul>
      <li><a href="createBatch">Create New Batch</a></li>
    </ul>
  </section>
  <section id = "content">
    <table border="1" width="100%">
      <thead><tr>
        <th>Batch ID</th><th>Recipe Name</th><th>Batch Status</th><th>Command</th>
      </tr></thead>
      <tbody id="batchlist">
    <c:forEach var="batch" items="${batchList}">
      <tr id = ${batch.batchSeq}>
        <td><a href="batchDetails?BSN=${batch.batchSeq}">${batch.batchId}</a></td>
        <td>${batch.recipeName}</td>
        <td>${batch.batchStatus}</td>
        <td>
          <select onchange="statusCommand(${batch.batchSeq},this.value)">
          <option value="">Select a command for this batch</option>
          <c:forEach var="command" items="${batch.listCommands}">
            <option value="${command}">${command}</option>
          </c:forEach>
          </select>
        </td> 
      </tr>
    </c:forEach>
      </tbody>
    </table>
  </section>
</body>
</html>
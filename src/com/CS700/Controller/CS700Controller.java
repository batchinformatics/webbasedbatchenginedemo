package com.CS700.Controller;
 
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.CS700.service.BatchService;
import com.CS700.dao.Batch;
import com.CS700.dao.Recipe;

@Controller
public class CS700Controller {
	static final Logger logger = LogManager.getLogger("CS700");

	@Autowired
	BatchService batchService; // autowired brings the batchService into the controller
	
	@RequestMapping("/batchList")
	public ModelAndView printBatchList() { //batchList
		logger.debug("SERVICE:" + batchService);
		System.out.println(logger.getName());
		List<Batch> batchList = batchService.getBatchList();
		return new ModelAndView("batchList", "batchList", batchList);
	} //batchList

	@RequestMapping("/batchDetails")
	public ModelAndView printBatchDetail(@RequestParam Map<String,String> allRequestParams) { // batchDetails
		logger.debug("SERVICE:" + batchService + "Params: " + allRequestParams);
		Batch batch = batchService.getBatchDetail(Integer.parseInt(allRequestParams.get("BSN")));
		return new ModelAndView("batchDetails", "batch", batch);		
	}  //batchDetails

  	
	@RequestMapping("/createBatch")
	public ModelAndView newBatchForm() { //createBatch
		logger.debug("SERVICE:" + batchService);
		List<Recipe> recipeList = batchService.getRecipeList();
		return new ModelAndView("createBatch", "recipeList", recipeList);
	} // end createBatch

	@RequestMapping("/newBatch")
	public String newBatch(@RequestParam Map<String,String> allRequestParams) {  // newBatch
		logger.debug("SERVICE:" + batchService);
		batchService.newBatch(allRequestParams.get("BATCH_ID").toUpperCase(),
                              allRequestParams.get("RECIPE_NAME"));
        //Simply redirect to batchList without the arguments in URL such that a reload causes an error
		return "redirect:batchList";
	} //end newBatch
	
}

package com.CS700.Controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.CS700.Notify;
import com.CS700.AckMsg;
import com.CS700.CmdMsg;
import com.CS700.dao.Batch;
import com.CS700.service.BatchService;

@Controller
@RestController
@EnableAsync
public class CS700WebsocketController {
	static final Logger logger = LogManager.getLogger("CS700ws");

	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@Autowired
	BatchService batchService; // autowired brings the batchService into the controller

	@MessageMapping("/ackMsg")  //This should receive an ackMsg then publish the updated batch object to all subscribers
  	public void processBatchAckMsg(AckMsg message) { //processBatchAckMsg
		logger.debug("Ack received BSN: [{}] ISN: [{}]",message.getBsn(),message.getIsn());
		batchService.ackInstruction(message.getBsn(),message.getIsn());
		return; //response;        
	} // end processBatchAckMsg

  	@MessageMapping("/batchCommand")  //This should receive a batch status command and publish the updated batch object to all subscribers
  	public void processBatchCommand(CmdMsg message) { //process batchCommand
  		logger.debug("Command [{}] received for BSN: [{}]",message.getCommand(),message.getBsn());
		Batch response;
		try {
		  response = batchService.commandBatchDetail(message.getBsn(),message.getCommand());
		}
		catch (java.lang.IndexOutOfBoundsException e) {
			logger.debug("Batch number [{}] does not exist.  Exception: {}",message.getBsn(),e.getMessage());
			response = new Batch();
			response.setBatchStatus("DELETED");
			response.setBatchSeq(message.getBsn());
			processBatchCommand(response);
			return; //response; //return an empty batch?;
		}
		return; //response; // Can be handled by batchlist and batchdetail;
  	} // end batchCommand

    @RequestMapping(value = "/notify", method = RequestMethod.POST, consumes="application/json",produces="application/json")
    public ResponseEntity<Notify> message(@RequestBody Notify msg){ //REST Endpoint
    logger.debug("Rest service invoked");
        Batch changedBatch = batchService.getBatchDetail(msg.getBsn());
		try{
			this.messagingTemplate.convertAndSend("/topic/bsn"+changedBatch.getBatchSeq(),changedBatch);
			this.messagingTemplate.convertAndSend("/topic/batchchanges",changedBatch);
			logger.debug("BSN [{}] status: [{}]",changedBatch.getBatchSeq(),changedBatch.getBatchStatus());
            msg.setStatus(HttpStatus.OK);
			msg.setResponse(String.format("Batch [%d] change to status [%s] pushed to client", changedBatch.getBatchSeq(),changedBatch.getBatchStatus()));
		}
		catch (Exception e)
		{
			logger.debug("Error sending message.  Error: [{}] Cause: [{}]",e.getStackTrace(), e.getCause());
			msg.setResponse(e.getMessage());
			msg.setStatus(HttpStatus.BAD_REQUEST);
		}
        return new ResponseEntity<Notify>(msg, HttpStatus.OK);
    } //REST service

  	public void processBatchCommand(Batch changedBatch) { //process batchCommand
		logger.debug("Unsolicited Change received for BSN: [{}]",changedBatch.getBatchSeq());
		try{
			this.messagingTemplate.convertAndSend("/topic/bsn"+changedBatch.getBatchSeq(),changedBatch);
			this.messagingTemplate.convertAndSend("/topic/batchchanges",changedBatch);
		}
		catch (Exception e)
		{
			logger.debug("Error sending message.  Error: [{}] Cause: [{}]",e.getStackTrace(), e.getCause());
		}
  	} // end batchCommand
  	
}
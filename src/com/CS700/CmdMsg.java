package com.CS700;

public class CmdMsg {

    /**
	 * 
	 */
	private Integer bsn;
    private String command;
    
    public CmdMsg() {
    }

    public CmdMsg(String command, Integer bsn) {
        this.bsn = bsn;
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public Integer getBsn() {
        return bsn;
    }

    public void setBsn(Integer bsn) {
        this.bsn = bsn;
    }

}

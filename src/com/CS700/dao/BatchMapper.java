package com.CS700.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import org.springframework.jdbc.core.RowMapper;

public class BatchMapper implements RowMapper<Batch> {
	public Batch mapRow(ResultSet rs, int rowNum) throws SQLException {
      Batch batch = new Batch();
      batch.setBatchSeq(rs.getInt("BATCH_SEQ"));
      batch.setBatchId(rs.getString("BATCH_ID"));
      batch.setBatchStatus(rs.getString("BATCH_STATUS"));
      batch.setRecipeName(rs.getString("RECIPE_NAME"));;
      batch.setRecipeVer(rs.getInt("RECIPE_VER"));;
      batch.setRecipeDesc(rs.getString("RECIPE_DESC"));
      batch.setListCommands(Arrays.asList(rs.getString("COMMANDS").split("[|]")));
      return batch;
   }
}


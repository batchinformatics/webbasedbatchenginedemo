package com.CS700.dao;

import java.util.List;

public class Batch {
	private Integer batchSeq;
    private String batchId;
    private String batchStatus;
    private String recipeName;
    private Number recipeVer;
    private String recipeDesc;
    private String lastStatus;
    private List<String> listCommands;
    public List<BatchInstruction> listInstructions;
    
	   public void setBatchSeq(Integer seq) {
	      this.batchSeq = seq;
	   }
	   public Integer getBatchSeq() {
	      return batchSeq;
	   }
	   public void setBatchId(String id) {
	      this.batchId = id;
	   }
	   public String getBatchId() {
		   return batchId;
	   }

	   public void setBatchStatus(String status) {
	      this.batchStatus = status;
	   }
	   public String getBatchStatus() {
	      return batchStatus;
	   }

	   public void setRecipeName(String name) {
	      this.recipeName = name;
	   }
	   public String getRecipeName() {
	     return recipeName;
	   }
	   
	   public void setRecipeVer(Number version) {
		      this.recipeVer = version;
	   }
	   public Number getRecipeVer() {
	     return recipeVer;
	   }
	   public void setRecipeDesc(String desc) {
		  this.recipeDesc = desc;
	   }
	   public String getRecipeDesc() {
	      return recipeDesc;
	   }
	   public String getLastStatus() {
		return lastStatus;
	   } 
	   public void setLastStatus(String lastStatus) {
		this.lastStatus = lastStatus;
	   }
	   
	   public void setListInstructions(List<BatchInstruction> instructList) {
		      this.listInstructions =instructList;
	   }

	   public List<BatchInstruction> getListInstructions() {
		      return listInstructions;
		   }
	   public void setListCommands(List<String> commandList) {
		      this.listCommands = commandList;
		   }

	   public List<String> getListCommands() {
		      return listCommands;
		   }

}

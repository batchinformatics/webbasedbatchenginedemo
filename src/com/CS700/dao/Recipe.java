package com.CS700.dao;

public class Recipe {
	private Integer recipeId;
    private String recipeName;
    private Integer recipeVer;
    private String recipeDesc;
    private String recipeCreateDate;

	public Integer getRecipeId() {
		return recipeId;
	}
	public void setRecipeId(Integer recipeId) {
		this.recipeId = recipeId;
	}
	public String getRecipeName() {
		return recipeName;
	}
	public void setRecipeName(String recipeName) {
		this.recipeName = recipeName;
	}
	public Integer getRecipeVer() {
		return recipeVer;
	}
	public void setRecipeVer(Integer recipeVer) {
		this.recipeVer = recipeVer;
	}
	public String getRecipeDesc() {
		return recipeDesc;
	}
	public void setRecipeDesc(String recipeDesc) {
		this.recipeDesc = recipeDesc;
	}
	public String getRecipeCreateDate() {
		return recipeCreateDate;
	}
	public void setRecipeCreateDate(String recipeCreateDate) {
		this.recipeCreateDate = recipeCreateDate;
	}
}

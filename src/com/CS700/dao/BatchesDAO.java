package com.CS700.dao;

import java.util.List;

public interface BatchesDAO {
	   /** 
	    * This is the method to be used to create
	    * a batch.
	    */
	   public Integer createBatch(String recipeName, String batchId);
	   /** 
	    * This is the method to be used to Get current information for a specific batch
	    */
	   public Batch getBatch(Integer BatchSeq);
	   /** 
	    * This is the method to be used to list all of the batches
	    */
	   public List<Batch> listBatches();
	   /** 
	    * This is the method to be used to list all of the recipes
	    */
//	   public List<Batch> listUpdatedBatches();
	   /** 
	    * This is the method to be used list batches that have been updated in the past ten seconds
	    */
	   public List<Recipe> listRecipes();
	   /** 
	    * This is the method to command a batch
	    */
	   public void commandBatch(Integer batchSeq, String command);

	   public void ackInstruction(Integer batchSeq, Integer instructSeq);

}

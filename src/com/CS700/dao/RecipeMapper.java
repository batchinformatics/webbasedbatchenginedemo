package com.CS700.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class RecipeMapper implements RowMapper<Recipe> {
	public Recipe mapRow(ResultSet rs, int rowNum) throws SQLException {
	      Recipe recipe = new Recipe();
	      recipe.setRecipeId(rs.getInt("RECIPE_ID"));
	      recipe.setRecipeName(rs.getString("RECIPE_NAME"));
	      recipe.setRecipeVer(rs.getInt("RECIPE_VER"));;
	      recipe.setRecipeDesc(rs.getString("RECIPE_DESC"));
	      recipe.setRecipeCreateDate(rs.getString("RECIPE_CREATE_DATE"));
	      return recipe;
	   }
	}

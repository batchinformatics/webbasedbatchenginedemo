package com.CS700.dao;

public class BatchInstruction {
	private Integer batchSeq;
    private Integer instructionSeq;
    private String instructText;
    private String acknowledged;
    private String activeTs;
	private String ackTs;
	
    public Integer getBatchSeq() {
		return batchSeq;
	}
	public void setBatchSeq(Integer batchSeq) {
		this.batchSeq = batchSeq;
	}
	public Integer getInstructionSeq() {
		return instructionSeq;
	}
	public void setInstructionSeq(Integer instructionSeq) {
		this.instructionSeq = instructionSeq;
	}
	public String getInstructText() {
		return instructText;
	}
	public void setInstructText(String instructText) {
		this.instructText = instructText;
	}
	public String getAcknowledged() {
		return acknowledged;
	}
	public void setAcknowledged(String acknowledged) {
		this.acknowledged = acknowledged;
	}
	public String getActiveTs() {
		return activeTs;
	}
	public void setActiveTs(String activeTs) {
		this.activeTs = activeTs;
	}
	public String getAckTs() {
		return ackTs;
	}
	public void setAckTs(String ackTs) {
		this.ackTs = ackTs;
	}

}

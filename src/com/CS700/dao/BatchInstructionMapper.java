package com.CS700.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BatchInstructionMapper implements RowMapper<BatchInstruction> {
	public BatchInstruction mapRow(ResultSet rs, int rowNum) throws SQLException {
	      BatchInstruction bi = new BatchInstruction();
	      bi.setBatchSeq(rs.getInt("BATCH_SEQ"));
	      bi.setInstructionSeq(rs.getInt("INSTRUCTION_SEQ"));
	      bi.setInstructText(rs.getString("INSTRUCT_TEXT"));
	      bi.setAcknowledged(rs.getString("ACKNOWLEDGED"));
	      bi.setActiveTs(rs.getString("ACTIVE_TS"));
	      bi.setAckTs(rs.getString("ACK_TS"));
	      return bi;
	   }
	}

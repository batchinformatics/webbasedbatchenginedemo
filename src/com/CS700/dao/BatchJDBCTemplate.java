package com.CS700.dao;

import java.sql.Types;
import java.util.Map;
import java.util.List;
import java.math.BigDecimal;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.beans.factory.annotation.Autowired;

import oracle.jdbc.OracleTypes;

public class BatchJDBCTemplate implements BatchesDAO {
	static final Logger logger = LogManager.getLogger("CS700");

	@Autowired
	 DataSource dataSource;  //Autowired brings the dataSource bean in from servlet.xml

//	private DataSource dataSource;
// An instance of SimpleDdbcCall for each method - gets initialized once and then re-used	 
   private SimpleJdbcCall getBatchCall;
   private SimpleJdbcCall getBatchListCall;
   private SimpleJdbcCall getRecipeListCall;
   private SimpleJdbcCall createBatchCall;
   private SimpleJdbcCall commandBatchCall;
   private SimpleJdbcCall ackInstructionCall;
  

   public Integer createBatch(String recipeName, String batchId) { //createBatch
	  if (this.createBatchCall == null){ //createBatchCall is null
		  this.createBatchCall =  new SimpleJdbcCall(dataSource)
                                      .withProcedureName("GENERATE_NEW_BATCH");
	  }//createBatchCall is null
	  SqlParameterSource in = new MapSqlParameterSource()
                                  .addValue("recipeName", recipeName)
                                  .addValue("batchId", batchId);
	  Map<String, Object> out  = createBatchCall.execute(in);
      return ((BigDecimal) out.get("BATCHSEQ")).intValueExact();  
      /*
       * After spending HOURS upon HOURS trying to figure out why jdbc is treating 
       * this int as a BigDecimal and trying to change the Oracle SP to make jdbc realize
       * is an int, I have given up and taken this approach.  I *think* the problem is 
       * related to the original sequence definition which was defined as unlimited, but
       * there's something somewhere that won't let go of that BigDecimal type.
       */
   } //end createBatch

   @SuppressWarnings("unchecked")
	public Batch getBatch(Integer batchSeq) { //getBatch
	   logger.debug("enter");
	   if (this.getBatchCall == null){ //getBatchCall is null
		   this.getBatchCall =  new SimpleJdbcCall(dataSource).
                  withProcedureName("GET_BATCH");
	   }//getBatchCall is null

	   SqlParameterSource in = new MapSqlParameterSource().
                              addValue("batchSeq", batchSeq);
	   getBatchCall.declareParameters(new SqlOutParameter("batchCursor",OracleTypes.CURSOR,new BatchMapper()));
	   getBatchCall.declareParameters(new SqlOutParameter("batchInstructionCursor",OracleTypes.CURSOR,new BatchInstructionMapper()));

	   Map<String, Object> result  = getBatchCall.execute(in);
       Batch batch = ((List<Batch>) result.get("batchCursor")).get(0); //There can only be one record in the result set
       batch.setListInstructions((List<BatchInstruction>) result.get("batchInstructionCursor"));
       for (BatchInstruction i : batch.getListInstructions())
       {
    	   logger.debug(i.getInstructionSeq() + " " + i.getInstructText() + " " + i.getAcknowledged());
       }
	   logger.debug("exit");
       return batch;
   }  //end getBatch

	@SuppressWarnings("unchecked")
	public List <Batch> listBatches() { //listBatches
	   if (this.getBatchListCall == null){ //getBatchListCall is null
	   this.getBatchListCall =  new SimpleJdbcCall(dataSource)
          .withProcedureName("GET_BATCH_LIST")
          .withoutProcedureColumnMetaDataAccess();
	   } //getBatchListCall is null
	   getBatchListCall.declareParameters(new SqlOutParameter("P_CURSOR", OracleTypes.CURSOR, new BatchMapper()));
	   Map<String,Object>  results = getBatchListCall.execute();
	   return ((List<Batch>) results.get("P_CURSOR"));
   }// end listBatches
   
   public void commandBatch(Integer batchSeq, String command) { //commandBatch
	   if (this.commandBatchCall == null){
		   this.commandBatchCall =  new SimpleJdbcCall(dataSource)
	          .withProcedureName("COMMAND_BATCH")
	          .withNamedBinding();
	   }
	   logger.debug("Command Batch batchSeq: " + batchSeq + " command: " + command);
	   MapSqlParameterSource in = new MapSqlParameterSource()
	                                  .addValue("batchSeq", batchSeq, Types.BIGINT)
	                                  .addValue("szCommand", command, Types.VARCHAR);
       commandBatchCall.execute(in);
       return;
   } //end commandBatch


   @SuppressWarnings("unchecked")
   public List<Recipe> listRecipes() { //listRecipes
	   if (this.getRecipeListCall == null){ //getRecipeListCall null
	   this.getRecipeListCall =  new SimpleJdbcCall(dataSource)
                                     .withProcedureName("GET_RECIPE_LIST")
                                     .withoutProcedureColumnMetaDataAccess();
	   }//getRecipeListCall null
	   getRecipeListCall.declareParameters(new SqlOutParameter("P_CURSOR", OracleTypes.CURSOR, new RecipeMapper()));
	   Map<String,Object>  results = getRecipeListCall.execute();
	   return ((List<Recipe>) results.get("P_CURSOR"));
   } //end listRecipes

@Override
	public void ackInstruction(Integer batchSeq, Integer instructSeq) {
	   if (this.ackInstructionCall == null){ //ackInstructionCall null
	       this.ackInstructionCall =  new SimpleJdbcCall(dataSource)
                                          .withProcedureName("ACKNOWLEDGE_INSTRUCTION")
                                          .withNamedBinding();
	   } //ackInstructionCall null
	   logger.debug("ackInstruction enter");
	   MapSqlParameterSource in = new MapSqlParameterSource()
                                     .addValue("batchSeq", batchSeq)
                                     .addValue("InstructionSeq", instructSeq);
	   ackInstructionCall.execute(in);
	   logger.debug("ackInstruction exit");
	   return;
	}	
}
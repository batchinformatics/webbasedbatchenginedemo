package com.CS700;

import org.springframework.http.HttpStatus;

public class Notify {

	private Integer bsn;
    private String message;
    private String response;
    private HttpStatus status;
    
    public Notify() {
    }

    public Notify(Integer bsn, String message, String response, HttpStatus status) {
        this.bsn = bsn;
        this.message = message;
        this.response = response;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    
    public Integer getBsn() {
        return bsn;
    }

    public void setBsn(Integer bsn) {
        this.bsn = bsn;
    }

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}
	
	
}

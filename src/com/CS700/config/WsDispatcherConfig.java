package com.CS700.config;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration 
@ComponentScan("com.CS700") 
@EnableWebMvc
@Import({ WebSocketConfig.class })

public class WsDispatcherConfig extends WebMvcConfigurerAdapter {
	static final Logger logger = LogManager.getLogger("CS700");
	   
	@Bean  
	public UrlBasedViewResolver setupViewResolver() {  
	    UrlBasedViewResolver resolver = new UrlBasedViewResolver();  
	    resolver.setPrefix("/WEB-INF/jsp/");  
	    resolver.setSuffix(".jsp");  
	    resolver.setViewClass(JstlView.class);  
	    return resolver;  
    }
	
	
}

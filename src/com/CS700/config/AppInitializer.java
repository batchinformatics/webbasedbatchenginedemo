package com.CS700.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class AppInitializer implements WebApplicationInitializer {

	@Override
    public void onStartup(ServletContext container) {
      // Create the 'root' Spring application context
      AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
      rootContext.register(AppConfig.class);

      // Manage the lifecycle of the root application context
      container.addListener(new ContextLoaderListener(rootContext));

      // Create the dispatcher servlet's Spring application context
      AnnotationConfigWebApplicationContext dispatcherContext =
        new AnnotationConfigWebApplicationContext();
      dispatcherContext.register(DispatcherConfig.class);

      // Register and map the dispatcher servlet
      Dynamic dispatcher = container.addServlet("CS700", new DispatcherServlet(dispatcherContext));
      dispatcher.setLoadOnStartup(1);
      dispatcher.addMapping("/batchList");
      dispatcher.addMapping("/batchDetails");
      dispatcher.addMapping("/createBatch");
      dispatcher.addMapping("/newBatch");

      AnnotationConfigWebApplicationContext wsDispatcherContext = new AnnotationConfigWebApplicationContext();
      wsDispatcherContext.register(WsDispatcherConfig.class);

      Dynamic wsDispatcher = container.addServlet("CS700ws", new DispatcherServlet(wsDispatcherContext));
      wsDispatcher.setLoadOnStartup(2);
      wsDispatcher.addMapping("/");
      wsDispatcher.addMapping("/notify");      
      
	}
		
}
package com.CS700.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.CS700.dao.BatchJDBCTemplate;
import com.CS700.service.BatchServiceImpl;


@Configuration
@EnableWebMvc
@ComponentScan("com.CS700.CS700Controller")
@ComponentScan(basePackages = {"com.CS700"} )
@Primary
public class DispatcherConfig extends WebMvcConfigurerAdapter {

	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	
	@Bean
	public BatchJDBCTemplate batchDao() {
		return new BatchJDBCTemplate();
	}
	
	@Bean
	public BatchServiceImpl batchService() {
		return new BatchServiceImpl();
	}
	
	@Bean
	public DriverManagerDataSource dataSource() {
	    DriverManagerDataSource ds = new DriverManagerDataSource();
	    ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
//	    ds.setUrl("jdbc:oracle:thin:@192.168.1.101:1521/WILLDEV");
	    ds.setUrl("jdbc:oracle:thin:@192.168.1.101:1522/WILLDEV2");
	    ds.setUsername("CS700");
	    ds.setPassword("cs700");
	    return ds;
	}
	
}
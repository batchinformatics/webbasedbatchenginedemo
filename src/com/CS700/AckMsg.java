package com.CS700;

public class AckMsg {

    private Integer bsn;
    private Integer isn;
    
    public AckMsg() {
    }

    public AckMsg(Integer bsn, Integer isn) {
        this.bsn = bsn;
        this.isn = isn;
    }

    public Integer getIsn() {
        return isn;
    }

    public void setIsn(Integer isn) {
        this.isn = isn;
    }

    public Integer getBsn() {
        return bsn;
    }

    public void setBsn(Integer bsn) {
        this.bsn = bsn;
    }

}

package com.CS700.service;

import java.util.List;

import com.CS700.dao.Batch;
import com.CS700.dao.Recipe;

public interface BatchService {

 public List<Batch> getBatchList();
  
 public Integer newBatch(String strBatchId, String strRecipeName);

 public List<Recipe> getRecipeList();

 public Batch getBatchDetail(Integer batchSeqNum);
 
 public Batch commandBatchDetail(Integer batchSeqNum, String strCommand);

 public void ackInstruction(Integer batchSeqNum, Integer instructionSeqNum);

}

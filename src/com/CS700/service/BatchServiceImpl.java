package com.CS700.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.CS700.dao.Batch;
import com.CS700.dao.BatchesDAO;
import com.CS700.dao.Recipe;

public class BatchServiceImpl implements BatchService {
	static final Logger logger = LogManager.getLogger("CS700");

	@Autowired
	BatchesDAO batchdao; //Autowired brings a BatchesDAO instance into service

	@Override
	public List<Batch> getBatchList() {
		return batchdao.listBatches();
	}

	@Override
	public List<Recipe> getRecipeList() {
		return batchdao.listRecipes();
	}

	@Override
	public Batch getBatchDetail(Integer batchSeqNum) {
		return batchdao.getBatch(batchSeqNum);
	}
	@Override 
	public Batch commandBatchDetail(Integer batchSeqNum, String strCommand){
		batchdao.commandBatch(batchSeqNum, strCommand);
		return batchdao.getBatch(batchSeqNum);
	}

	@Override
	public Integer newBatch(String strBatchId, String strRecipeName) {
		Integer newBSN = batchdao.createBatch(strRecipeName, strBatchId);
		logger.info("New batch created with BSN: " + newBSN);
		return newBSN;
	}

	@Override
	public void ackInstruction(Integer batchSeqNum, Integer instructionSeqNum) {
	    batchdao.ackInstruction(batchSeqNum,instructionSeqNum);
	}
}